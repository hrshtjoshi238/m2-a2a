import React,{Component} from 'react';
import http from '../services/httpService';
import {Link} from 'react-router-dom';
class ViewShops extends Component{
    state={
        shopDetails:[]
    }
    async componentDidMount(){
        try{
            let response = await http.get('/shops');
            let {data} = response;
            this.setState({shopDetails:[...data]});
        }catch(err){
            console.log(err);
            this.setState({err:err});
        }
        
    }
    render(){
        let {shopDetails=[],err}=this.state;
        return(
            <div className="container" style={{marginTop:"50px"}}>
                <h3 className="display-3">Shops</h3>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Shop Id</th>
                            <th scope="col">Shop Name</th>
                            <th scope="col">Rent</th>
                            <th scope="col">Purchases</th>
                            <th scope="col">Total Purchaes</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            shopDetails.map((x)=>(
                                <tr key={x.shopId}>
                                    <td>{x.shopId}</td>
                                    <td>{x.name}</td>
                                    <td>{x.rent}</td>
                                    <td><Link to={`/purchase?shop=st${x.shopId}`} style={{textDecoration:"none",cursor:"pointer",color:"purple",fontWeight:"bolder"}}>Click Me</Link></td>
                                    <td><Link to={`/totalPurchase/shopAllProds/${x.shopId}`} style={{textDecoration:"none",cursor:"pointer",color:"brown",fontWeight:"bolder"}}>Click Me</Link></td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
export default ViewShops;