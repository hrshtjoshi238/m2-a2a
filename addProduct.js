import React,{Component} from 'react';
import http from '../services/httpService'
class AddProduct extends Component{
    state={
        productDetails:{
            productName:"",
            category:"",
            description:"",
        },
        errors:{}
    }
    handleChange=(e)=>{
        let {currentTarget:input} =e;
        let s1 = {...this.state};
        s1.productDetails[input.name]=input.value;
        this.setState(s1); 
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    validateAll=()=>{
        let {productName,category,description}=this.state.productDetails;
        let errors={};
        errors.productName=productName?"":"Product Name is not entered";
        errors.category=!category?"Category is not entered":"";
        errors.description=!description?"Description is not entered":"";
        return errors;
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let {id}=this.props.match.params
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            !id?this.addProduct('/products',this.state.productDetails):this.editProduct(`/products/${id}`,this.state.productDetails);
        }
    }
    async componentDidMount(){
        let {id}=this.props.match.params;
        let response=await http.get(`/product/${id}`);
        let {data}=response;
        console.log(data);
        if(data) {
            this.setState({productDetails:data,dataAvail:true});
        }
    }
    async addProduct(url,obj){
        try {
            let response = await http.post(url,obj);
            let {data} = response;
            alert("Product added successfully");
            window.location="/products";
        }
        catch(err){
            console.log(err);
        }
    }
    async editProduct(url,obj){
        try {
            console.log("I am in edit");
            let response = await http.put(url,obj);
            console.log(response);
            let {data} = response;
            alert("Product edited successfully");
            window.location="/products";
        }
        catch(err){
            console.log(err);
        }
    }
    getText=(name,value,label,placeholder,disabled,error)=>{
        console.log(error);
        return <React.Fragment>
            <label htmlFor={name}>{label}</label>
            <input
            className="form-control"
            id={name}
            type="text"
            disabled={disabled}
            name={name}
            placeholder={placeholder}
            value={value}
            onChange={this.handleChange}/>
            {error?<div className="col-12 form-group" style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{error}</div>:""}
        </React.Fragment>
    }
    render(){
        let {id}=this.props.match.params;
        let {errors}=this.state;
        let {productName,category,description}=this.state.productDetails;
        return(
            <div className="container" style={{marginTop:"50px"}}>
                <h3 className="display-3">{id?"Edit Product":"Add Product"}</h3>
                <div className="form-group">
                    {this.getText("productName",productName,"Product Name","Enter Product Name...",id?true:false,errors.productName)}
                </div>
                <div className="form-group mt-4">
                    {this.getText("category",category,"Category","Enter Category...",id?true:false,errors.category)}
                </div>
                <div className="form-group mt-4">
                    {this.getText("description",description,"Description","Enter Description...",false,errors.description)}
                </div>
                <div className="form-group mt-4">
                    <button className="btn btn-primary" onClick={this.handleSubmit}>{id?"Edit Product":"Add Product"}</button>
                </div>
            </div>
        )
    }
}
export default AddProduct;