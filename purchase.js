import React,{Component} from 'react';
import http from '../services/httpService';
import queryString from 'query-string';
class Purchase extends Component{
    state={
        purchaseDetails:[]
    }
    handleOptionChange=(options)=>{
        console.log(options);
        this.callURL("/",options);
    }
    callURL=(url,options)=>{
        let searchStr=this.makeSearchStr(options);
        console.log(searchStr);
        this.props.history.push({
            pathName:url,
            search:searchStr
        });
    };
    async componentDidMount(){
        let queryParams=queryString.parse(this.props.location.search);
        let searchString=this.makeSearchStr(queryParams);
        console.log(searchString);
        try{
            let response = await http.get(`/purchase?${searchString}`);
            let {data} = response;
            this.setState({purchaseDetails:[...data]})    
        } catch (error) {
            console.log(error);
        }
    }
    makeSearchStr=(options)=>{
        let {shop,product,sort}=options;
        let searchStr="";
        searchStr=this.addToQueryString(searchStr,'shop',shop);
        searchStr=this.addToQueryString(searchStr,'product',product);
        return searchStr;
    }
    addToQueryString=(searchStr,name,value)=>{
        return value?
        searchStr?`${searchStr}&${name}=${value}`
        :`${name}=${value}`
        :searchStr;
    }
    render(){
        let {name,id}=this.props.match.params;
        let {purchaseDetails=[]}=this.state;
        console.log(purchaseDetails);
        return(
            <div className="container" style={{marginTop:"50px"}}>
                <h3 className="display-3">Purchase</h3>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Purchase Id</th>
                            <th scope="col">Shop Id</th>
                            <th scope="col">Product Id</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    {
                            purchaseDetails.map((x,index)=>(
                                <tr key={index}>
                                    <td>{x.purchaseId}</td>
                                    <td>{x.shopId}</td>
                                    <td>{x.productid}</td>
                                    <td>{x.quantity}</td>
                                    <td>{x.price}</td>
                                </tr>
                            ))
                    }
                    </tbody>
                </table>
            </div>
        )
        
    }
}
export default Purchase;