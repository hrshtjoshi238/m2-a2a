import React,{Component} from 'react';
import {Link} from 'react-router-dom';
import {Navbar,Nav,NavDropdown} from 'react-bootstrap';
class NavBar extends Component{
    render(){
        return(
            <Navbar collapseOnSelect expand="lg" bg="secondary" variant="light">
                <Link className="navbar-brand text-weight-bold text-light" to="/" style={{fontFamily:"Sigmar One, cursive"}}>Shops</Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <NavDropdown title="Shops" id="basic-nav-dropdown">
                            <NavDropdown.Item href="/shops">View</NavDropdown.Item>
                            <NavDropdown.Item href="/shops/add">Add</NavDropdown.Item>
                        </NavDropdown>
                        <NavDropdown title="Products" id="basic-nav-dropdown">
                            <NavDropdown.Item href="/products">View</NavDropdown.Item>
                            <NavDropdown.Item href="/products/add">Add</NavDropdown.Item>
                        </NavDropdown>
                        <Link className="nav-link" to="/purchase2">Purchase</Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}
export default NavBar;