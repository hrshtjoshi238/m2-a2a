import React,{Component} from 'react';
import http from '../services/httpService';
class OptionFilter extends Component{
    state={
        shopsArr:[],
        productsArr:[],
        sortArr:['QtyAsc','QtyDesc','ValueAsc','ValueDesc']
    }
    async componentDidMount(){
        try{
            let response2  = await http.get(`/products`);
            let response3  = await http.get(`/shops`);
            this.setState(
                {
                    productsArr:[...response2.data],
                    shopsArr:[...response3.data],
                }

            )    
        } catch (error) {
            console.log(error);
        }
    }
    handleChange=(e)=>{
        const {currentTarget:input}=e;
        let options={...this.props.options};
        input.type==="checkbox"?
        options[input.name]=this.updateCBs(
            options[input.name],
            input.checked,
            input.value
        )
        :options[input.name]=input.value;
        this.props.onOptionsChange(options);
    };
    updateCBs=(inpValues,checked,value)=>{
        let inpArr=inpValues?inpValues.split(","):[];
        if(checked) inpArr.push(value);
        else{
            let index=inpArr.findIndex((ele)=>ele===value);
            if(index>=0)inpArr.splice(index,1);
        }
        return inpArr.join(",");
    };
    clearFilters=()=>window.location=("/purchase2");
    createCheck=(arr,values,name,label)=>{
        return <ul className="list-group">
                    <li class="list-group-item"><b>{label}</b></li>
                    {
                       arr.map((x)=>(
                            <li className="list-group-item form-check pl-5" style={{paddingLeft:"40px"}}>
                                <input
                                className="form-check-input"
                                value={x.id}
                                id={x.id}
                                type="checkbox"
                                name={name}
                                checked={values.find((x1)=>x1===x.id)}
                                onChange={this.handleChange}
                                onOptionsChange={this.handleOptionChange}
                                />
                                <label className="form-check-label" htmlFor={x.id}>{x.name}</label>
                            </li>
                ))
            }
            </ul>
    }
    createDrpDwn=(arr,value,name,label,disabledTxt)=>{
        return <React.Fragment>
            <label style={{fontWeight:'bolder'}}>{label}</label>
            <select className="form-control" name={name} value={value} onChange={this.handleChange}>
            <option disabled value="">{disabledTxt}</option>
            {
                arr.map((x,index)=><option value={x.id} key={index}>{x.name}</option>)
            }
        </select>
        </React.Fragment>
    }
    createRadios=(arr,values,name,label)=>{
        return <ul className="list-group">
                    <li class="list-group-item"><b>{label}</b></li>
                    {
                       arr.map((x)=>(
                            <li className="list-group-item form-check" style={{paddingLeft:"40px"}}>
                                <input
                                className="form-check-input"
                                value={x}
                                id={x}
                                type="radio"
                                name={name}
                                checked={values==x}
                                onChange={this.handleChange}
                                />
                                <label className="form-check-label" htmlFor={x}>{x}</label>
                            </li>
                ))
            }
            </ul>
    }
    render(){
        let {productsArr=[],shopsArr=[],sortArr} =this.state;
        let {product="",shop="",sort=""}=this.props.options;
        productsArr=productsArr.map(x=>{
            let json={
                name:x.productName,
                id:'pr'+x.productId
            }
            return json;
        });
        shopsArr=shopsArr.map(x=>{
            let json={
                name:x.name,
                id:'sh'+x.shopId
            }
            return json;
        });
        console.log(productsArr,shopsArr);
        return(
            <div className="container bg-light p-4">
               <div className="row">
                    <div className="col-12">
                        {this.createCheck(productsArr,product.split(","),"product","Product")}
                    </div>
                    <div className="col-12"><br/></div> 
                    <div className="col-12">
                        {this.createDrpDwn(shopsArr,shop,"shop","Shop","Select Shop")}
                    </div>
                    <div className="col-12"><br/></div>
                    <div className="col-12">
                        {this.createRadios(sortArr,sort,"sort","Sort")}
                    </div>
                    <div className="col-12">
                        <button className="btn btn-light border-dark float-right" onClick={()=>this.clearFilters()}>Clear</button>
                    </div>
                </div> 
            </div>
        )
    }
} 
export default OptionFilter;