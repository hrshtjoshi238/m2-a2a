import React,{Component} from 'react';
import {Redirect,Route,Switch} from 'react-router-dom';
import AddProduct from './addProduct';
import AddShop from './addShop';
import NavBar from './navBar';
import Purchase2 from './pruchase2';
import Purchase from './purchase';
import TotalPurchase from './totalPurchase';
import ViewProducts from './viewProducts';
import ViewShops from './viewShops';
class MainComp extends Component{
    render(){
        return(
            <React.Fragment>
                <NavBar/>
                <Switch>
                    <Route path="/totalPurchase/:name/:id"  render={(props)=><TotalPurchase {...props}/>}/>
                    <Route path="/editProduct/:id" component={AddProduct}/>
                    <Route path="/purchase2" component={Purchase2}/>
                    <Route path="/purchase" component={Purchase}/>
                    <Route path="/shops/add" component={AddShop}/>
                    <Route path="/products/add" component={AddProduct}/>
                    <Route path="/shops" component={ViewShops}/>
                    <Route path="/products" component={ViewProducts}/>
                    <Redirect from="/" to="/shops"/>;      
                </Switch>
            </React.Fragment>
        )
    }
}
export default MainComp;