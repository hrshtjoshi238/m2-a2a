import React,{Component} from 'react';
import http from '../services/httpService';
class AddShop extends Component{
    state={
        shopDetails:{
            name:"",
            rent:"",
        },
        errors:{}
    }
    handleChange=(e)=>{
        let {currentTarget:input} =e;
        let s1 = {...this.state};
        s1.shopDetails[input.name]=input.value;
        this.setState(s1); 
    }
    isError=(errors)=>{
        let keys=Object.keys(errors);
        return keys.reduce((acc,curr)=>errors[curr]?++acc:acc,0);
    }
    validateAll=()=>{
        let {name,rent}=this.state.shopDetails;
        let regEx1=/^[0-9]+$/
        let errors={};
        errors.name=name?"":"Shop Name is not entered";
        errors.rent=!rent?"Rent is not entered"
                    :!regEx1.test(rent)?"Enter Valid Amount"
                    :"";
        return errors;
    }
    handleSubmit=(e)=>{
        e.preventDefault();
        let errors=this.validateAll();
        if(this.isError(errors)){
            let s1={...this.state};
            s1.errors=errors;
            this.setState(s1);
        }
        else{
            this.addShop('/shops',this.state.shopDetails)
        }
    }
    async addShop(url,obj){
        try {
            let response = await http.post(url,obj);
            let {data} = response;
            alert("Shop added successfully");
            window.location="/shops";
        }
        catch(err){
            console.log(err);
        }
    }
    getText=(name,value,label,placeholder,disabled,error)=>{
        console.log(error);
        return <React.Fragment>
            <label htmlFor={name}>{label}</label>
            <input
            className="form-control"
            id={name}
            type="text"
            disabled={disabled}
            name={name}
            placeholder={placeholder}
            value={value}
            onChange={this.handleChange}/>
            {error?<div className="col-12 form-group" style={{color:"rgb(204, 0, 102)",background:"rgb(255, 153, 204)",padding:"12px"}}>{error}</div>:""}
        </React.Fragment>
    }
    render(){
        let {errors}=this.state;
        let {name,rent}=this.state.shopDetails;
        return(
            <div className="container" style={{marginTop:"50px"}}>
                <h3 className="display-3">Add Shop</h3>
                <div className="form-group">
                    {this.getText("name",name,"Shop Name","Enter Shop Name...",false,errors.shopName)}
                </div>
                <div className="form-group mt-4">
                    {this.getText("rent",rent,"Rent","Enter Rent...",false,errors.rent)}
                </div>
                <div className="form-group mt-4">
                    <button className="btn btn-primary" onClick={this.handleSubmit}>Add Shop</button>
                </div>
            </div>
        )
    }
}
export default AddShop;