import React,{Component} from 'react';
import http from '../services/httpService';
import {Link} from 'react-router-dom';
class ViewProducts extends Component{
    state={
        productsDetails:[]
    }
    async componentDidMount(){
        try{
            let response = await http.get('/products');
            let {data} = response;
            this.setState({productsDetails:[...data]});
        }catch(err){
            console.log(err);
            this.setState({err:err});
        }
        
    }
    handleEdit=(id)=>{
        console.log(id);
        this.props.history.push(`/editProduct/${id}`)
    };
    render(){
        let {productsDetails=[],err}=this.state;
        return(
            <div className="container" style={{marginTop:"50px"}}>
                <h3 className="display-3">Products</h3>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">Product Id</th>
                            <th scope="col">Product Name</th>
                            <th scope="col">Category</th>
                            <th scope="col">Description</th>
                            <th scope="col">Purchases</th>
                            <th scope="col">Total Purchaes</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            productsDetails.map((x)=>(
                                <tr key={x.productId}>
                                    <td>{x.productId}</td>
                                    <td>{x.productName}</td>
                                    <td>{x.category}</td>
                                    <td>{x.description}</td>
                                    <td><Link to={`/purchase?product=pr${x.productId}`} style={{textDecoration:"none",cursor:"pointer",color:"purple",fontWeight:"bolder"}}>Click Me</Link></td>
                                    <td><Link to={`/totalPurchase/prodAllShops/${x.productId}`} style={{textDecoration:"none",cursor:"pointer",color:"brown",fontWeight:"bolder"}}>Click Me</Link></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <button class="btn btn-warning btn-sm text-center" onClick={()=>this.handleEdit(x.productId)}><i class="fa fa-edit fa-lg"></i></button>
                                        </div>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}
export default ViewProducts;