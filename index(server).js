const e = require('express');
const express = require('express');
const app = express();
app.use(express.json());
const {shopDetails,updateShop} = require('./data');



app.use((req,res,next)=>{
    res.header("Access-Control-Allow-Origin","*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    res.header(
        "Access-Control-Expose-Headers",
        "X-Auth-Token"
    );
    res.header("Access-Control-Allow-Methods", "PUT, POST, GET, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Credentials",true);
    next();
});


/********************REST API's********************** */


//GET API's
app.get('/shops',(req,res)=>{
    let shops = [...shopDetails.shops]
    console.log(shops);
    res.send(shops);
});

app.get('/products',(req,res)=>{
    console.log(shopDetails.products);
    res.send(shopDetails.products);
});

app.get('/product/:id',(req,res)=>{
    let id=req.params.id;
    let product = shopDetails.products.find(x=>x.productId===parseInt(id));
    if(product){
        console.log(product);
        res.send(product);
    }
    else{
        res.status(404).send("Product not found")
    }
    
});


app.get('/purchase',(req,res)=>{
    let shop = req.query.shop;
    let product = req.query.product;
    let sort = req.query.sort;
    console.log(shop);
    console.log(product);
    let outArr = shopDetails.purchases;
    if(shop){
        let shopArr=shop.split(',');
        outArr=outArr.filter(x=>shopArr.find(x2=>parseInt(x2.charAt(2))===x.shopId));
    }
    if(product){
        let productArr=product.split(',');
        outArr=outArr.filter(x=>productArr.find(x2=>parseInt(x2.charAt(2))===x.productid));
    }
    if(sort){
        if(sort==="QtyAsc"||sort==="QtyDesc"){
            sort==="QtyAsc"?outArr.sort((x,y)=>x.quantity-y.quantity):outArr.sort((x,y)=>y.quantity-x.quantity);
        }
        
        if(sort==="ValueAsc"||sort==="ValueDesc"){
            sort==="ValueAsc"?outArr.sort((x,y)=>(x.quantity*x.price)-(y.quantity*y.price)):outArr.sort((x,y)=>(y.quantity*y.price)-(x.quantity*x.price));
        }

    }
    console.log(outArr);
    res.send(outArr);
});

app.get('/purchase/shops/:id',(req,res)=>{
    let id = req.params.id;
    if(shopDetails.shops.find(sh=>sh.shopId===parseInt(id))){
        let resPurchase = shopDetails.purchases.filter(sh=>sh.shopId===parseInt(id));
        console.log(resPurchase);
        res.send(resPurchase);
    }
    else{
        res.status(404).send('Error!!!Shop Id not found...');
    }
});

app.get('/purchase/products/:id',(req,res)=>{
    let id = req.params.id;
    if(shopDetails.products.find(pd=>pd.productId===parseInt(id))){
        let resPurchase = shopDetails.purchases.filter(pd=>pd.productid===parseInt(id));
        console.log(resPurchase);
        res.send(resPurchase);
    }
    else{
        res.status(404).send('Error!!!Product Id not found...');
    }
});

app.get('/totalPurchase/shop/:id',(req,res)=>{
    let id = req.params.id;
    let products = shopDetails.purchases.filter(x=>x.shopId===parseInt(id));
    let productsArr=[];
    products.forEach(x=>{
        if(!productsArr[x.productid]){
            productsArr[x.productid]={
                id:x.productid,
                productName:shopDetails.products[shopDetails.products.findIndex(x2=>x2.productId===parseInt(x.productid))].productName,
                quantity:x.quantity,
                price : x.price,
                totalSale : x.quantity*x.price
            }
        }
        else{
            productsArr[x.productid]={
                id:x.productid,
                productName:shopDetails.products[shopDetails.products.findIndex(x2=>x2.productId===parseInt(x.productid))].productName,
                quantity:x.quantity+productsArr[x.productid].quantity,
                price : x.price,
                totalSale : (x.quantity+productsArr[x.productid].quantity)*x.price
            }
        }
    });
    productsArr=productsArr.filter(x=>{
        if(x){
            return true;
        }
    });
    console.log(productsArr);
    res.send(productsArr);
});

app.get('/totalPurchase/product/:id',(req,res)=>{
    let id = req.params.id;
    let products = shopDetails.purchases.filter(x=>x.productid===parseInt(id));
    let shopsArr=[];
    products.forEach(x=>{
        if(!shopsArr[x.shopId]){
            shopsArr[x.shopId]={
                id:x.shopId,
                shopName:shopDetails.shops[shopDetails.shops.findIndex(x2=>x2.shopId===parseInt(x.shopId))].name,
                quantity:x.quantity,
                price : x.price,
                totalSale : x.quantity*x.price
            }
        }
        else{
            shopsArr[x.shopId]={
                id:x.shopId,
                shopName:shopDetails.shops[shopDetails.shops.findIndex(x2=>x2.shopId===parseInt(x.shopId))].name,
                quantity:x.quantity+shopsArr[x.shopId].quantity,
                price : x.price,
                totalSale : (x.quantity+shopsArr[x.shopId].quantity)*x.price
            }
        }
    });
    shopsArr=shopsArr.filter(x=>{
        if(x){
            return true;
        }
    });
    console.log(shopsArr);
    res.send(shopsArr);
});

//PUT API's
app.put('/products/:id',(req,res)=>{
    let id = req.params.id;
    let body = req.body;
    console.log("In Server Edit");
    if(shopDetails.products.find(x=>x.productId===parseInt(id))){
        let index = shopDetails.products.findIndex(x=>x.productId===parseInt(id));
        shopDetails.products[index]={
            productId:id,
            productName:shopDetails.products[index].productName,
            ...body
        }
        updateShop(shopDetails);
        res.send(shopDetails.products[index]);
    }
    else{
        res.status(404).send('Error!! Product not found');
    }
});


//POST API's
app.post('/shops',(req,res)=>{
    let maxId = shopDetails.shops.reduce((acc,curr)=>curr.shopId>acc?curr.shopId:acc,0);
    let body = req.body;
    let shop = {
        shopId : maxId+1,
        ...body,
    }
    shopDetails.shops.push(shop);
    res.send(shop);
});

app.post('/products',(req,res)=>{
    let maxId = shopDetails.products.reduce((acc,curr)=>curr.productId>acc?curr.productId:acc,0);
    let body = req.body;
    let product = {
        productId : maxId+1,
        ...body,
    }
    shopDetails.products.push(product);
    res.send(product);
});

app.post('/purchase',(req,res)=>{
    let maxId = shopDetails.purchases.reduce((acc,curr)=>curr.purchaseId>acc?curr.purchaseId:acc,0);
    let body = req.body;
    let purchase = {
        purchaseId : maxId+1,
        ...body
    };
    shopDetails.purchases.push(purchase);
    res.send(purchase);
});

const port = process.env.PORT || 2310;
app.listen(port,(req,res)=>console.log(`Server is running on port ${port}`));

