import React,{Component} from 'react';
import http from '../services/httpService';
class TotalPurchase extends Component{
    state={
        purchaseDetails:[]
    }
    async componentDidMount(){
        let {name,id}=this.props.match.params;
        if(name==="prodAllShops"){
            try {
                let response = await http.get(`/totalPurchase/product/${id}`);
                let {data} = response;
                this.setState({purchaseDetails:[...data]})    
            } catch (error) {
                console.log(error);
            }
            
        }
        else{
            try {
                let response = await http.get(`/totalPurchase/shop/${id}`);
                let {data} = response;
                this.setState({purchaseDetails:[...data]})    
            } catch (error) {
                console.log(error);
            }
        }
    }
    render(){
        let {name,id}=this.props.match.params;
        let {purchaseDetails=[]}=this.state;
        console.log(purchaseDetails);
        return(
            <div className="container" style={{marginTop:"50px"}}>
                <h3 className="display-3">Total Purchase</h3>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col">{name==="shopAllProds"?"Product Id":"Shop Id"}</th>
                            <th scope="col">{name==="shopAllProds"?"Product Name":"Shop Name"}</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Price</th>
                            <th scope="col">Value</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            purchaseDetails.map((x,index)=>(
                                <tr key={index}>
                                    <td>{x.id}</td>
                                    <td>{name==="shopAllProds"?x.productName:x.shopName}</td>
                                    <td>{x.quantity}</td>
                                    <td>{x.price}</td>
                                    <td>{x.totalSale}</td>
                                </tr>
                            ))
                        }
                    </tbody>
                </table>
            </div>
        )
        
    }
}
export default TotalPurchase;